<?php
/**
 * Created by PhpStorm.
 * User: jila
 * Date: 24/08/16
 * Time: 1:40 PM
 */

namespace BigCommerce;

use Silex;
use Symfony\Component\HttpFoundation\Request;
use Abraham\TwitterOAuth\TwitterOAuth;

require_once (dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php');

$app = new Silex\Application();

/** / will respond with Try	/hello/:name as text */
$app->get('/', function() use($app) {
    return 'Try /hello/:name';
});

/** /hello/BarackObama will respond with Hello	BarackObama as text */
$app->get('/hello/{name}', function($name) use ($app) {
    if (!isset($name)) {
        return $app->redirect('/');
    }
    return 'Hello ' . $app->escape($name);
});

/**
 * histogram/Ferrari will respond with a JSON structure displaying the
 * number of tweets per hour of the day
 */
$app->get('/histogram/{name}', function($name) use($app) {

    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
    $timeline   = new Twitter\Timeline($connection, COUNT);

    /** send the request to get the user's tweets */
    $result = $timeline->get($name);

    /** Filter user timeline and return array of all tweet dates */
    $filter = new Twitter\Filter();
    $result = $filter->created($result);

    /** Aggregator through array of dates and group them by hour */
    $aggregator = new Aggregator();
    $result  = $aggregator->groupByHour($result);

    return $app->json($result);
});

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    $error = ['message' => $e->getMessage()];

    return $app->json($error, $code);
});
$app->run();
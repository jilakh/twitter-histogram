##Twitter Histogram
An API to return hour-tweet counts for a given user.

###Requirement
You need to create a user on twitter and create your consumer credentials (CONSUMER_KEY and CONSUMER_SECRET) in the [Application Management](https://apps.twitter.com/).

You need the following applications installed in your system:

    PHP>5.6
    git
    nginx
    php-fpm

    sudo apt-get install php git nginx php-cli php-fpm

##Installation  
Clone this project 

```
git clone https://jilakh@bitbucket.org/jilakh/twitter-histogram.git
```

```cd twitter-histogram```

Then rename `config.php.dist` to `config.php` and add your CONSUMER_KEY and CONSUMER_SECRET

Install composer
 
```curl https://getcomposer.org/composer.phar -o bin/composer```

and run 

```chmod +x bin/composer ```

Then run 

```bin/composer install```

To run tests 

```bin/phpunit```

Change the root of the project in the web-server (nginx) configuration to absolute path to `twitter-histogram/web` (e.g. /home/jila/workspace/twitter-histogram/web)

This is what your nginx config in sites-enabled would look like: (i.e. /etc/nginx/sites-enabled/twitterhist.dev)

    server {
        listen          80;
        server_name     twitterhist.dev;
        root /home/jila/workspace/twitter-histogram/web;
        index index.php index.html index.htm;
    
        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }
    
        location ~ \.php$ {
                    fastcgi_connect_timeout  3s;     # default of 60s is just too long
                    fastcgi_read_timeout  10s;       # default of 60s is just too long
                    include fastcgi_params;
                    fastcgi_index index.php;
                    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                    fastcgi_pass  unix:/var/run/php5-fpm.sock;
        }
    }

Note that this config assumes the twitterhist.dev hostname is added to /etc/hosts like:
 
    127.0.0.1       twitterhist.dev

####Available endpoints
/

/hello/{name}

/histogram/{name} -> returns a JSON-encoded array containing hour as key and tweet counts as value for a given user 

####Design decision
Twitter Histogram is designed to be compatible with SOLID principles.

each class has a Single responsibility `Twitter\Timeline` is responsible to get the tweets of a specific user. `Twitter\Filter` is responsible to filter all the 

`created_at` from the tweets and return an array of dates. `Aggregator` is responsible to aggregate number of tweets base on the hour.

Interfaces are used so in the future Twitter can be replaced with another implementation or completely different social network (e.g. Facebook)
as long as it implements the same interface.

Dependencies are injected to constructor of classes.

####Shortcuts/Compromises
Shortcuts and compromises due to restrictions in time
 
 
  1 - Twitter api does not allow more the 100 request per hour, as a result `Twitter\Timeline` can get only 200 tweets. `Twitter\Timeline` 
       should create background job to gracefully generate histogram. It could instantly use the first 200 tweets to generate the histogram
       and cache it for example in `Redis` then in the background job gracefully update the cache.
  
  2 - Used `abraham/twitteroauth` to connect to twitter API
  
  3 - Due to time the test for `Twitter\Timeline` is left
  
  
   
  
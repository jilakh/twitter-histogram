<?php
/**
 * Created by PhpStorm.
 * User: jila
 * Date: 24/08/16
 * Time: 11:33 PM
 */

namespace BigCommerce;

use BigCommerce\AggregatorInterface;

class Aggregator implements AggregatorInterface
{
    /**
     * @param array $dates
     * @return array
     */
    public function groupByHour(array $dates)
    {
        $groupedBy = [];

        foreach ($dates as $date) {
            $date = new \DateTime($date);
            $key = $date->format('H');
            $groupedBy[$key] = isset($groupedBy[$key]) ? $groupedBy[$key] + 1 : 1;
        }

        ksort($groupedBy);

        return $groupedBy;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jila
 * Date: 24/08/16
 * Time: 5:27 PM
 */
namespace BigCommerce\Twitter;

use BigCommerce\TimelineInterface;
use Abraham\TwitterOAuth\TwitterOAuth;

class Timeline implements TimelineInterface
{
    protected $connection;
    protected $count;

    /**
     * @param TwitterOAuth $connection
     * @param int $count
     */
    public function __construct(TwitterOAuth $connection, $count = 200)
    {
        $this->connection = $connection;
        $this->count      = $count;
    }

    /**
     * This method returns histogram of the specified user
     *
     * @param string $name
     *
     * @return array
     * @throws \Exception
     */
    public function get($name)
    {
        $tweets = $this->userTimeline($name);

        if (count($tweets) < 1) {
            return [];
        }

        return $tweets;
    }

    /**
     * @param $name
     * @return array
     * @throws \Exception
     */
    protected function userTimeline($name)
    {
        $params = [
            'trim_user'   => 1,
            'include_rts' => 1,
            'user_id'     => $name,
            'screen_name' => $name,
            'count'       => $this->count
        ];

        $tweets = $this->connection->get('statuses/user_timeline', $params);

        if ($this->connection->getLastHttpCode() != 200) {
            throw new \Exception($tweets->errors[0]->message, $tweets->errors[0]->code);
        }

        return $tweets;
    }
}
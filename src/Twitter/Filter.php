<?php
/**
 * Created by PhpStorm.
 * User: jila
 * Date: 24/08/16
 * Time: 11:59 PM
 */

namespace BigCommerce\Twitter;

use BigCommerce\FilterInterface;

class Filter implements FilterInterface
{
    /**
     * filter all the created_at from the tweets and return an array of dates
     *
     * @param array $tweets
     * @return array
     */
    public function created(array $tweets)
    {
        $dates = [];

        foreach($tweets as $tweet) {
            $tweet = (array) $tweet;
            $dates[] = $tweet['created_at'];
        }

        return $dates;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jila
 * Date: 24/08/16
 * Time: 11:26 PM
 */

namespace BigCommerce;

interface AggregatorInterface
{
    public function groupByHour(array $dates);
}
<?php
/**
 * Created by PhpStorm.
 * User: jila
 * Date: 25/08/16
 * Time: 12:00 AM
 */

namespace BigCommerce;

interface FilterInterface
{
    public function created(array $input);
}
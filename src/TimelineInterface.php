<?php
/**
* Created by PhpStorm.
* User: jila
* Date: 24/08/16
* Time: 1:40 PM
*/

namespace BigCommerce;

interface TimelineInterface
{
    /**
     * This method returns timeline of the specified user
     *
     * @param string $name
     *
     * @return array
     */
    public function get($name);
}
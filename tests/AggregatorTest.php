<?php
/**
 * Created by PhpStorm.
 * User: jila
 * Date: 25/08/16
 * Time: 8:18 PM
 */

use BigCommerce\Aggregator;
use PHPUnit\Framework\TestCase;


class AggregatorTest extends TestCase
{
    public function testGroupByHour()
    {
        $input = [
            'Wed Aug 24 17:38:52 +0000 2016',
            'Thu Aug 25 15:59:52 +0000 2016',
            'Wed Aug 24 17:02:42 +0000 2016',
            '25-08-2016 17:22'
        ];
        $aggregator = new Aggregator();
        $got = $aggregator->groupByHour($input);

        $expected = [
            "15" => 1,
            "17" => 3
        ];

        $this->assertSame($expected, $got);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jila
 * Date: 25/08/16
 * Time: 8:56 PM
 */

use PHPUnit\Framework\TestCase;
use BigCommerce\Twitter\Filter;

class FilterTest extends TestCase
{
    public function testCreatedArray()
    {
        $input = [
            [
                "created_at" => "Wed Aug 24 17:38:52 +0000 2016",
                "id_str" => "768502849449762816",
            ],
            [
                "listed_count" => 26,
                "created_at" => "Tue Mar 24 08:43:52 +0000 2009",
            ]
        ];

        $filter = new Filter();
        $got = $filter->created($input);

        $expected = [
            "Wed Aug 24 17:38:52 +0000 2016",
            "Tue Mar 24 08:43:52 +0000 2009"
        ];

        $this->assertSame($expected, $got);
    }

    public function testCreatedObject()
    {
        $tweet1 = new stdClass();
        $tweet1->created_at = "Wed Aug 24 17:38:52 +0000 2016";
        $tweet1->id_str     = "768502849449762816";

        $tweet2 = new stdClass();
        $tweet2->created_at   = "Tue Mar 24 08:43:52 +0000 2009";
        $tweet2->listed_count = "768502849449762816";

        $input = [
            $tweet1,
            $tweet2
        ];

        $filter = new Filter();
        $got = $filter->created($input);

        $expected = [
            "Wed Aug 24 17:38:52 +0000 2016",
            "Tue Mar 24 08:43:52 +0000 2009"
        ];

        $this->assertSame($expected, $got);
    }
}